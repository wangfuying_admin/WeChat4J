package wx;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;

public class IoUtil {

	public static String ReadFile(String filePath) {
		File file = new File(filePath);
		StringBuffer sb = new StringBuffer();
		if (file.exists() && file.isFile()) {
			try {
				FileInputStream in = new FileInputStream(file);
				byte[] buffer = new byte[1024];
				int len = -1;
				while((len = in.read(buffer)) != -1) {
					sb.append(new String(buffer, 0, len, Charset.forName("utf-8")));
				}
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String content = IoUtil.ReadFile("a");
		System.out.println(content);
	}

}
