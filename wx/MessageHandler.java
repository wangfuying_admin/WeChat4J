package wx;

import java.util.Date;
import java.util.Map;

public class MessageHandler {
	private WxBot wxBot = null;
	
	public MessageHandler(WxBot wxBot) {
		this.wxBot = wxBot;
	}
	
	
	public WxBot getWxBot() {
		return wxBot;
	}

	public void setWxBot(WxBot wxBot) {
		this.wxBot = wxBot;
	}


	public String onTextMessage(Map<String, String> requestMap) {
		// 默认返回的文本消息内容
		String respContent = "未知的消息类型！";
		// 发送方帐号
		String fromUserName = requestMap.get("FromUserName");
		// 开发者微信号
		String toUserName = requestMap.get("ToUserName");

		// 回复文本消息
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);

		respContent = "您发送的是文本消息！";

		// 设置文本消息的内容
		textMessage.setContent(respContent);
		// 将文本消息对象转换成xml
		String respXml = MessageUtil.messageToXml(textMessage);
		return respXml;
	}

	public String onImageMessage(Map<String, String> requestMap) {
		// 默认返回的文本消息内容
		String respContent = "未知的消息类型！";
		// 发送方帐号
		String fromUserName = requestMap.get("FromUserName");
		// 开发者微信号
		String toUserName = requestMap.get("ToUserName");

		// 回复文本消息
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
		// 文本消息
		respContent = "您发送的是图片消息！";
		// 设置文本消息的内容
		textMessage.setContent(respContent);
		// 将文本消息对象转换成xml
		String respXml = MessageUtil.messageToXml(textMessage);
		return respXml;
	}

	public String onVoiceMessage(Map<String, String> requestMap) {
		// 默认返回的文本消息内容
		String respContent = "未知的消息类型！";
		// 发送方帐号
		String fromUserName = requestMap.get("FromUserName");
		// 开发者微信号
		String toUserName = requestMap.get("ToUserName");

		// 回复文本消息
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
		// 文本消息
		respContent = "您发送的是语音消息！";
		// 设置文本消息的内容
		textMessage.setContent(respContent);
		// 将文本消息对象转换成xml
		String respXml = MessageUtil.messageToXml(textMessage);
		return respXml;
	}

	public String onVideoMessage(Map<String, String> requestMap) {
		// 默认返回的文本消息内容
		String respContent = "未知的消息类型！";
		// 发送方帐号
		String fromUserName = requestMap.get("FromUserName");
		// 开发者微信号
		String toUserName = requestMap.get("ToUserName");

		// 回复文本消息
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
		// 文本消息
		respContent = "您发送的是视频消息！";
		// 设置文本消息的内容
		textMessage.setContent(respContent);
		// 将文本消息对象转换成xml
		String respXml = MessageUtil.messageToXml(textMessage);
		return respXml;
	}

	public String onLocationMessage(Map<String, String> requestMap) {
		// 默认返回的文本消息内容
		String respContent = "未知的消息类型！";
		// 发送方帐号
		String fromUserName = requestMap.get("FromUserName");
		// 开发者微信号
		String toUserName = requestMap.get("ToUserName");

		// 回复文本消息
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
		// 文本消息
		respContent = "您发送的是位置消息！";
		// 设置文本消息的内容
		textMessage.setContent(respContent);
		// 将文本消息对象转换成xml
		String respXml = MessageUtil.messageToXml(textMessage);
		return respXml;
	}

	public String onLinkMessage(Map<String, String> requestMap) {
		// 默认返回的文本消息内容
		String respContent = "未知的消息类型！";
		// 发送方帐号
		String fromUserName = requestMap.get("FromUserName");
		// 开发者微信号
		String toUserName = requestMap.get("ToUserName");

		// 回复文本消息
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
		// 文本消息
		respContent = "您发送的是链接消息！";
		// 设置文本消息的内容
		textMessage.setContent(respContent);
		// 将文本消息对象转换成xml
		String respXml = MessageUtil.messageToXml(textMessage);
		return respXml;
	}

	public String onShortVideoMessage(Map<String, String> requestMap) {
		// 默认返回的文本消息内容
		String respContent = "未知的消息类型！";
		// 发送方帐号
		String fromUserName = requestMap.get("FromUserName");
		// 开发者微信号
		String toUserName = requestMap.get("ToUserName");

		// 回复文本消息
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
		// 文本消息
		respContent = "您发送的是短视频消息！";
		// 设置文本消息的内容
		textMessage.setContent(respContent);
		// 将文本消息对象转换成xml
		String respXml = MessageUtil.messageToXml(textMessage);
		return respXml;
	}

	public String onEventSubscribeMessage(Map<String, String> requestMap) {
		// 默认返回的文本消息内容
		String respContent = "未知的消息类型！";
		// 发送方帐号
		String fromUserName = requestMap.get("FromUserName");
		// 开发者微信号
		String toUserName = requestMap.get("ToUserName");

		// 回复文本消息
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
		// 文本消息
		respContent = "感谢您的关注!" + fromUserName;
		// 设置文本消息的内容
		textMessage.setContent(respContent);
		// 将文本消息对象转换成xml
		String respXml = MessageUtil.messageToXml(textMessage);
		return respXml;
	}

	public String onEventClickMessage(Map<String, String> requestMap) {
		// 默认返回的文本消息内容
		String respContent = "未知的消息类型！";
		// 发送方帐号
		String fromUserName = requestMap.get("FromUserName");
		// 开发者微信号
		String toUserName = requestMap.get("ToUserName");

		// 回复文本消息
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
		// 文本消息
		respContent = "点击 " + requestMap.get("Event") + " !" + fromUserName;
		// 设置文本消息的内容
		textMessage.setContent(respContent);
		// 将文本消息对象转换成xml
		String respXml = MessageUtil.messageToXml(textMessage);
		return respXml;
	}

	public String onEventLocationMessage(Map<String, String> requestMap) {
		// 默认返回的文本消息内容
		String respContent = "未知的消息类型！";
		// 发送方帐号
		String fromUserName = requestMap.get("FromUserName");
		// 开发者微信号
		String toUserName = requestMap.get("ToUserName");

		// 回复文本消息
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
		// 文本消息
		respContent = "更新地址信息!" + fromUserName;
		// 设置文本消息的内容
		textMessage.setContent(respContent);
		// 将文本消息对象转换成xml
		String respXml = MessageUtil.messageToXml(textMessage);
		return respXml;
	}

	public String onEventScanMessage(Map<String, String> requestMap) {
		// 默认返回的文本消息内容
		String respContent = "未知的消息类型！";
		// 发送方帐号
		String fromUserName = requestMap.get("FromUserName");
		// 开发者微信号
		String toUserName = requestMap.get("ToUserName");

		// 回复文本消息
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
		// 文本消息
		respContent = "扫描二维码!" + fromUserName;
		// 设置文本消息的内容
		textMessage.setContent(respContent);
		// 将文本消息对象转换成xml
		String respXml = MessageUtil.messageToXml(textMessage);
		return respXml;
	}

	public String onEventUnsubscribeMessage(Map<String, String> requestMap) {
		// 默认返回的文本消息内容
		String respContent = "未知的消息类型！";
		// 发送方帐号
		String fromUserName = requestMap.get("FromUserName");
		// 开发者微信号
		String toUserName = requestMap.get("ToUserName");

		// 回复文本消息
		TextMessage textMessage = new TextMessage();
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		textMessage.setCreateTime(new Date().getTime());
		textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
		// 文本消息
		respContent = "感谢您的取消关注!" + fromUserName;
		// 设置文本消息的内容
		textMessage.setContent(respContent);
		// 将文本消息对象转换成xml
		String respXml = MessageUtil.messageToXml(textMessage);
		return respXml;
	}
	
	public String onEventView(Map<String, String> requestMap) {
		return "success";
	}
	
	public String onEventSendTemplateMessageDone(Map<String, String> requestMap) {
		return "success";
	}
	
	public String onGetAccessToken(String accesstoken) {
		return null;
	}
}
